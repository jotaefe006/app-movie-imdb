## Projec create with vue 3, vuex, vue-router, sass, css3, html5
## Create by Jhon Frey Diaz, software developer. San Juan de Pasto, Colombia
## In this project we used the api movies' IMDB
## My contact is: +57 3173187766, email jotaefe006@gmail.com 
## This project is public an free!
# app-movie

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
