import { createApp } from 'vue';
import './styles/styles.scss';

import router from './router/index.js';
import {store} from './store/index.js';
import App from './App';


const app = createApp(App);

app.use(router).use(store).mount('#app')
