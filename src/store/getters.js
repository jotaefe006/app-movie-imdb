export default {

    /**
     * Getter: Popular movies
     *
     * @param {*} state
     * @returns
     */
     movies:(state) => {
        return state.movies;
    },
    /**
     * Getter: Movie
     *
     * @param {*} state
     * @returns
     */
     movie:(state) => {
      return state.movie;
  }
    
  }