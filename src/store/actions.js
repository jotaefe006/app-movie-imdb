import axios from 'axios';
const baseUrl = 'https://api.themoviedb.org/3/';
const apiKey = '44fc540e11d02be081cba8a772caf160';
export default {

    /**
     * * Action: get popular movies
     * 
     * @param {*} {commit,getters} 
     */
    async getPopularMovies({commit}) {
        const URL = `${baseUrl}discover/movie?sort_by=popularity.desc&api_key=${apiKey}`;
        await axios.get(URL)
            .then(response => {
                commit('SET_POPULAR_MOVIES',response.data.results);
            }).catch(err => {
                console.log(err)
            })
    },
    /**
     * * Action: get movie by id
     * 
     * @param {*} {commit,getters} 
     */
     async getMovie({commit}, id) {
        const URL = `${baseUrl}movie/${id}?api_key=${apiKey}`;
        await axios.get(URL)
            .then(response => {
                commit('SET_MOVIE',response.data);
            }).catch(err => {
                console.log(err)
            })
    },

  }