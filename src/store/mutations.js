export default {

    /**
     * Mutation: SET_POPULAR_MOVIES
     *
     * @param {*} state
     * @param {*} 
     */
     SET_POPULAR_MOVIES(state, movies){
        state.movies = movies
    },
    /**
     * Mutation: SET_MOVIE
     *
     * @param {*} state
     * @param {*} 
     */
     SET_MOVIE(state, movie){
        state.movie = movie
    },
}